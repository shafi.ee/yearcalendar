//
//  ViewController.swift
//  YearCalendar
//
//  Created by Amin Shafiee on 24/06/2020.
//  Copyright © 2020 Amin Shafiee. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var calendarHolder: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        let calendarYearView = MPCalendarYearView(frame: CGRect(x: 10, y: 10, width: UIScreen.main.bounds.size.width-20, height: 400))
        calendarYearView.baseConfig(pastOffSet: -12, nextOffset: 12)
        calendarHolder.addSubview(calendarYearView)
        
        calendarYearView.translatesAutoresizingMaskIntoConstraints = false
        calendarYearView.topAnchor.constraint(equalTo: self.calendarHolder.topAnchor).isActive = true
        calendarYearView.bottomAnchor.constraint(equalTo: self.calendarHolder.bottomAnchor).isActive = true
        calendarYearView.leadingAnchor.constraint(equalTo: self.calendarHolder.leadingAnchor).isActive = true
        calendarYearView.trailingAnchor.constraint(equalTo: self.calendarHolder.trailingAnchor).isActive = true
        
    }


}

