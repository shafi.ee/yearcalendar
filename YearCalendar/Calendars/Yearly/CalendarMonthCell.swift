//
//  CalendarMonthCell.swift
//  myperiod
//
//  Created by Amin Shafiee on 23/06/2020.
//  Copyright © 2020 Amin Shafiee. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarMonthCell: JTACMonthCell {

    static let identifier = "CalendarMonthCell"
    static let identifierRTL = "CalendarMonthCellRTL"
    
    
    @IBOutlet weak var monthTittleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

}
