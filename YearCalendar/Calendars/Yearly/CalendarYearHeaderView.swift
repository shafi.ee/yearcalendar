//
//  CalendarYearHeaderView.swift
//  myperiod
//
//  Created by Amin Shafiee on 23/06/2020.
//  Copyright © 2020 Amin Shafiee. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarYearHeaderView: JTACMonthCell {

    static let identifier = "CalendarYearHeaderView"
    static let identifierRTL = "CalendarYearHeaderViewRTL"
    
    @IBOutlet weak var yearTitleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.yearTitleLbl.font = .header2
    }

}
