//
//  MPCalendarYearView.swift
//  myperiod
//
//  Created by Amin Shafiee on 23/06/2020.
//  Copyright © 2020 Amin Shafiee. All rights reserved.
//

import UIKit
import Foundation
import JTAppleCalendar

class MPCalendarYearView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var calendarView: UIView!
    @IBOutlet weak var collectionView: JTACYearView!
    
    var offsetForPreviousMonth = 0, offSetForNextMonth = 0
    var embeddedCalendarFormatter = Date().getFormattedDate(format: .embeddedCalendar)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubview()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        initSubview()
    }

    func initSubview() {
        let nib = UINib(nibName: "MPCalendarYearView", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        contentView.backgroundColor = .clear
        addSubview(contentView)
    }
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func baseConfig(pastOffSet: Int, nextOffset: Int) {
        collectionView.register(UINib(nibName: "CalendarMonthCell", bundle: nil), forCellWithReuseIdentifier: CalendarMonthCell.identifier)
        collectionView.register(UINib(nibName: "CalendarYearHeaderView", bundle: nil), forCellWithReuseIdentifier: CalendarYearHeaderView.identifier)
        collectionView.calendarDelegate = self
        collectionView.calendarDataSource = self
        offsetForPreviousMonth = pastOffSet
        offSetForNextMonth = nextOffset
//        collectionView.scrollToDate(Date.dayFromToday(offset: 0, dateComponent: .day))
        
//        if Locale.userPreferred.isRTL {
//            collectionView.semanticContentAttribute = .forceRightToLeft
//        } else {
//            collectionView.semanticContentAttribute = .forceLeftToRight
//        }
        collectionView.reloadData()
    }

}

extension MPCalendarYearView: JTACYearViewDelegate, JTACYearViewDataSource {
    // Drawing for a whole month cell
    func calendar(_ calendar: JTACYearView, cellFor item: Any, at date: Date, indexPath: IndexPath) -> JTACMonthCell {
        if item is Month {
            let cell = calendar.dequeueReusableJTAppleMonthCell(withReuseIdentifier: CalendarMonthCell.identifier, for: indexPath) as! CalendarMonthCell
            embeddedCalendarFormatter.dateFormat = "MMM"
            cell.monthTittleLbl.text = embeddedCalendarFormatter.string(from: date)
            cell.backgroundColor = .red
            return cell
        } else {
            let cell = calendar.dequeueReusableJTAppleMonthCell(withReuseIdentifier: CalendarYearHeaderView.identifier, for: indexPath) as! CalendarYearHeaderView
            embeddedCalendarFormatter.dateFormat = "yyyy"
            cell.yearTitleLbl.text = embeddedCalendarFormatter.string(from: date)
            
            cell.backgroundColor = .blue
            return cell
        }
    }



    func configureCalendar(_ calendar: JTACYearView) -> (configurationParameters: ConfigurationParameters, months: [Any]) {
        
        let df = DateFormatter()
        df.dateFormat = "yyyy MM dd"
            let sDate = df.date(from: "2019 01 01")!
            let eDate = df.date(from: "2021 05 31")!

            let configParams = ConfigurationParameters(startDate: sDate,
                                                       endDate: eDate,
                                                       numberOfRows: 6,
                                                       calendar: Calendar(identifier: .gregorian),
                                                       generateInDates: .forAllMonths,
                                                       generateOutDates: .tillEndOfGrid,
                                                       firstDayOfWeek: .sunday,
                                                       hasStrictBoundaries: true)
        

        // Get year data
        let dataSource = calendar.dataSourcefrom(configurationParameters: configParams)

        // Modify the data source to include a String every 12 data elements.
        // This string type will be used to add a header.
        var modifiedDataSource: [Any] = []
        for index in (0..<dataSource.count) {
            if index % 12 == 0 { modifiedDataSource.append("Year") }
            modifiedDataSource.append(dataSource[index])
        }
        return (configParams, modifiedDataSource)
    }


    // Drawing for individual item in a month cell.
    func calendar(_ calendar: JTACYearView, monthView: JTACCellMonthView, drawingFor rect: CGRect, with date: Date, dateOwner: DateOwner, monthIndex index: Int) {
        embeddedCalendarFormatter.dateFormat = "d"
        let dateString = embeddedCalendarFormatter.string(from: date)

        let paragraphStyle = NSMutableParagraphStyle()
        
        paragraphStyle.alignment = .center
        dateString.draw(in: rect, withAttributes: [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.paragraphStyle: paragraphStyle
        ])
    }

    func calendar(_ calendar: JTACYearView, sizeFor item: Any) -> CGSize {
        if item is Month {
            let width = (calendar.frame.width - 30 ) / 3
            let height = width
            return CGSize(width: width, height: height)
        } else {
            let width = calendar.frame.width - 30
            let height:CGFloat  = 40
            return CGSize(width: width, height: height)
        }
    }
    
}


extension Date {
enum EnumDateFormatter: String {
    case embeddedCalendar = "yyyy MM dd"
}
    func getFormattedDate(format: EnumDateFormatter, noChange: Bool = false) -> DateFormatter {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format.rawValue
        
            dateformat.calendar = Calendar(identifier: .gregorian)
            dateformat.locale = Locale(identifier: "en_US_POSIX")
            dateformat.timeZone = TimeZone(secondsFromGMT: 0)
        
        return dateformat
    }
}
